/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of map-editor. */

/* map-editor is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* map-editor is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with map-editor.  If not, see <http://www.gnu.org/licenses/>. */

#include "../inc/core.h"

/* normals */
#include <stdlib.h>
#include <unistd.h>

/* x related */
#include <xcb/xcb.h>
#include <X11/Xlib.h>

/* vulkan related? */

xcb_connection_t *conn;
xcb_window_t window_id;
xcb_void_cookie_t window_cookie;
const xcb_setup_t *setup;
xcb_screen_iterator_t iter;
xcb_screen_t *screen;

void init_xcb(void)
{
  conn =
    xcb_connect(NULL,           /* use env DISPLAY */
                NULL            /* discard screen number */
                );
  window_id =
    xcb_generate_id(conn);

  setup =
    xcb_get_setup(conn);
  iter =
    xcb_setup_roots_iterator(setup);
  screen =
    iter.data;

  xcb_event_mask_t valwin[] =
    {
     XCB_EVENT_MASK_KEY_PRESS |
     XCB_EVENT_MASK_KEY_RELEASE |
     XCB_EVENT_MASK_BUTTON_PRESS |
     XCB_EVENT_MASK_BUTTON_RELEASE
    };
  
  window_cookie =
    xcb_create_window(conn,
                      XCB_COPY_FROM_PARENT, /* depth */
                      window_id,
                      screen->root, /* parent */
                      100, /* x */
                      100, /* y */
                      640, /* width */
                      480, /* height */
                      1,   /* border width */
                      XCB_WINDOW_CLASS_INPUT_OUTPUT, /* _class */
                      screen->root_visual, /* visual */
                      XCB_CW_EVENT_MASK, /* value_mask */
                      valwin /* value_list */);
  xcb_void_cookie_t map_cookie =
    xcb_map_window(conn, window_id);

  xcb_flush(conn);
  options.conn = conn;
}
