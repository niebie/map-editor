/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of map-editor. */

/* map-editor is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* map-editor is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with map-editor.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>

#include "../inc/grid.h"
#include "../inc/common.h"

#define MIN_TEXTURES 8

static struct{
  SDL_Renderer *renderer;
}grid_metadata;

/* should be a hash map probably. */
struct grid_texture{
  char *filename;
  SDL_Texture *texture;
  int id;
};

static struct grid_texture *textures;

struct grid_item{
  /* struct grid_texture *texture; */
  SDL_Texture *texture;
  int x;
  int y;
};

struct grid_item **grid;

static int grid_width, grid_height;
static int tile_width, tile_height;
static bool gridlines;

static void dump_options(void)
{
  printf("running with options:\n");
#define X(option){printf( #option ":%s\n", option == true ? "true" : "false");}
  X(gridlines);
#undef X
}

static void decode_options(int options)
{
  if((options & GRID_LINES) == GRID_LINES){
    gridlines = true;
  }else{
    gridlines = false;
  }
}

void init_grid(SDL_Renderer *renderer, int x, int y, int tw, int th, int options)
{
  printf("init'ing grid to size %d x %d\n", x, y);
  grid = xmalloc(sizeof(struct grid_item *) * x);
  for(int i = 0;
      i < x;
      i++){
    grid[i] = xmalloc(sizeof(struct grid_item) * y);
  }

  grid_width = x;
  grid_height = y;
  tile_width = tw;
  tile_height = th;

  decode_options(options);

  /* textures = xmalloc(sizeof(struct grid_texture) * MIN_TEXTURES); */
  grid_metadata.renderer = renderer;
  printf("grid init'd\n");
}

void delete_grid(void)
{
  for(int i = 0;
      i < grid_height;
      i++){
    free(grid[i]);
  }
  free(grid);
}

#define NO_FORMAT 0

void set_tile(int x, int y, char *file)
{
  printf("trying to set tile (%d, %d)\n", x, y);
  if(file == NULL){
    /* fatal("no file given you dipshit.\n"); */
  }
  
  /* SDL_CreateTexture(options.renderer, */
  /*                   NO_FORMAT, */
  /*                   SDL_TEXTUREACCESS_STATIC, */
  /*                   tile_width, */
  /*                   tile_height); */

  SDL_Surface *surface =
    SDL_CreateRGBSurface(0,     /* defunct flags */
                         tile_width, tile_height,
                         4,     /* depth */
                         100,   /* red */
                         100,     /* green */
                         100,     /* blue */
                         SDL_ALPHA_OPAQUE    /* alpha */
                         );

  if(surface == NULL){
    printf("issue with tile (%d, %d)\n", x, y);
    printf("SDL reports %s\n", SDL_GetError());
    return;
  }

  SDL_Texture *tex =
    SDL_CreateTextureFromSurface(grid_metadata.renderer,
                                 surface);
  SDL_FreeSurface(surface);

  /* todo: check or something */
  /* todo: lookup cache prob */
  grid[x][y].texture = tex;
  printf("grid:%d\n", grid);
  printf("grid[x]:%d\n", grid[x]);
  printf("grid[x][y]:%d\n", grid[x][y]);
  grid[x][y].x = x;
  grid[x][y].y = y;
  printf("set tile (%d, %d, %d, %d)\n", x, y, tile_width, tile_height);
}

void draw_grid(void)
{
  for(int i = 0;
      i < grid_width;
      i++){
    for(int j = 0;
        j < grid_height;
        j++){
      struct grid_item *item = &grid[i][j];
      SDL_Rect dst;
      dst.x = item->x * (tile_width + TILE_DIV) + TILE_DIV;
      dst.y = item->y * (tile_height + TILE_DIV) + TILE_DIV;
      dst.w = tile_width;
      dst.h = tile_height;
      int succ = SDL_RenderCopy(grid_metadata.renderer,
                                item->texture,
                                NULL, &dst);
      if(succ != 0){
        printf("sdl reported error rendering tile (%d, %d)\n", i, j);
        printf("error string:%s\n", SDL_GetError());
      }else{
      }
    }
  }
}

void grid_click_down(SDL_Event *e)
{
  printf("[%d] MB DOWN (%d, %d)\n",
         e->button.button,
         e->button.x,
         e->button.y);

  // 0-2 3-77 78-80
  //  |   -|-|-|-|-|-|-|
  /* int modx = (e->button.x-3) % 75; */
  /* int divx = (e->button.x-3) / 75; */
  /* int modxd = (e->button.x-3) % (75 + TILE_DIV); */
  
  /* printf(" modx=[%d]\n", modx); */
  /* printf(" divx=[%d]\n", divx); */
  /* printf("modxd=[%d]\n", modxd); */

  int remx = (e->button.x-1) % tile_width;
  int remy = (e->button.y-1) % tile_height;
  
  
  int divx = (e->button.x-1) / tile_width;
  int divy = (e->button.y-1) / tile_height;

  printf("tile clicked:(%d, %d)\n", divx, divy);
}

void grid_click_move(SDL_Event *e)
{
  
}

void grid_click_up(SDL_Event *e)
{
  printf("[%d] MB UP (%d, %d)\n",
         e->button.button,
         e->button.x,
         e->button.y);
}
