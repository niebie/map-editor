/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of map-editor. */

/* map-editor is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* map-editor is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with map-editor.  If not, see <http://www.gnu.org/licenses/>. */

#include <unistd.h>
#include <stdbool.h>

#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>

#include <SDL.h>
#include <SDL_events.h>

#include "../inc/core.h"
#include "../inc/grid.h"

static struct {
  
} keymaps[] = {

};

static void handle_event(SDL_Event *e)
{
  /* switch on enum SDL_EventType */
  switch(e->type){
  case SDL_KEYDOWN:
    printf("[%c] KEY DOWN\n", e->key.keysym.sym);
    break;
  case SDL_KEYUP:
    printf("[%c] KEY UP\n", e->key.keysym.sym);
    break;
  case SDL_QUIT:
    printf("got told to quit.\n");
    exit(EXIT_SUCCESS);
    break;
  case SDL_MOUSEBUTTONDOWN:
    grid_click_down(e);
    break;
  case SDL_MOUSEBUTTONUP:
    grid_click_up(e);
    break;
  case SDL_MOUSEMOTION:
    grid_click_move(e);
    break;
  default:
    printf("unhandled event.\n");
    break;
  }
}

void xcb_event_loop(void)
{
  xcb_key_symbols_t *theKeySyms = xcb_key_symbols_alloc(options.conn);

  /* see: xcbneko.c, demo in libxcb */
  /* later: xcb_key_symbols_free( keysyms ); */
  /* later: xcb_refresh_keyboard_mapping ( keysyms, mappingEvent ); */
  
  xcb_generic_event_t *e;
  while(true){
    e = xcb_poll_for_event(options.conn);
    if(e==NULL){
      if(xcb_connection_has_error(options.conn)){
        printf("xcb connection died.\n");
        exit(EXIT_SUCCESS);
      }
    }else{
      /* typedef struct { */
      /*     uint8_t   response_type;  /\**< Type of the response *\/ */
      /*     uint8_t  pad0;           /\**< Padding *\/ */
      /*     uint16_t sequence;       /\**< Sequence number *\/ */
      /*     uint32_t pad[7];         /\**< Padding *\/ */
      /*     uint32_t full_sequence;  /\**< full sequence *\/ */
      /* } xcb_generic_event_t; */
      printf("-----\n");
      /* printf("xcb sent us an event somehow\n"); */
      /* printf("looks like this:\n"); */
      /* printf("response_type:[%d]\n", e->response_type); */
      /* printf("pad0:[%d]\n", e->pad0); */
      /* printf("sequence:[%d]\n", e->sequence); */
      /* /\* printf("pad:[%d]\n", e->pad); *\/ */
      /* printf("full_sequence:[%d]\n", e->full_sequence); */

      switch(e->response_type){
      case XCB_KEY_PRESS: {
        xcb_key_press_event_t *ev = (xcb_key_press_event_t *)e;
        printf("**key press.**\n");
        /* printf("detail:[%d]\n", ev->detail); */
        xcb_keysym_t theKeySym = xcb_key_press_lookup_keysym(theKeySyms, ev, 1);

        printf("from lookup:[%d]\n", theKeySym);
        break;
      }
      case XCB_KEY_RELEASE:{
        xcb_key_release_event_t *ev = (xcb_key_release_event_t *)e;
        printf("**key release.**\n");
        /* printf("detail:[%d]\n", ev->detail); */

        xcb_keysym_t theKeySym = xcb_key_release_lookup_keysym(theKeySyms, ev, 1);

        printf("from lookup:[%d]\n", theKeySym);
        break;
      }
      case XCB_BUTTON_PRESS:{
        xcb_button_press_event_t *ev = (xcb_button_press_event_t *)e;
        printf("**button press.**\n");
        printf("detail:[%d]\n", ev->detail);
        break;
      }
      case XCB_BUTTON_RELEASE:{
        xcb_button_release_event_t *ev = (xcb_button_release_event_t *)e;
        printf("**button release.**\n");
        printf("detail:[%d]\n", ev->detail);
        break;
      }
      default:
        printf("**unknown event type.**\n");
        break;
      }
    }

    update();
    render();
    sleep(0);
  }
}

void SDL_event_loop(void)
{
  SDL_Event e;
  while(true){
    if(SDL_PollEvent(&e) == 1){
      handle_event(&e);
    }

    update();
    render();

    SDL_Delay(10);
  }
}

void event_loop(void)
{
  /* SDL_event_loop(); */
  xcb_event_loop();
}
