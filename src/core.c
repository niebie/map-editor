/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of map-editor. */

/* map-editor is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* map-editor is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with map-editor.  If not, see <http://www.gnu.org/licenses/>. */

#include "../inc/grid.h"
#include "../inc/core.h"
#include "../inc/xcb_util.h"

struct settings options;

static void init_tiles(void)
{
  for(int i = 0;
      i < GRID_WIDTH;
      i++){
    for(int j = 0;
        j < GRID_HEIGHT;
        j++){
      set_tile(i, j, NULL);
    }
  }
}

static void init_sdl(void)
{
  int x_size = GRID_WIDTH * TILE_WIDTH + (TILE_DIV * (GRID_WIDTH + 1));
  int y_size = GRID_HEIGHT * TILE_HEIGHT + (TILE_DIV * (GRID_HEIGHT + 1));
  SDL_Init(SDL_INIT_VIDEO);
  options.window =
    SDL_CreateWindow("map-editor",
                     0, 0,
                     x_size, y_size,
                     SDL_WINDOW_INPUT_GRABBED);
  if(options.window == NULL){
    printf("issue with the window yo\n");
    exit(EXIT_FAILURE);
  }

  options.renderer =
    SDL_CreateRenderer(options.window,
                       -1,
                       SDL_RENDERER_ACCELERATED);
  if(options.renderer == NULL){
    printf("issue with the renderer yo\n");
    exit(EXIT_FAILURE);
  }

  init_grid(options.renderer,
            GRID_WIDTH,
            GRID_HEIGHT,
            TILE_WIDTH,
            TILE_HEIGHT,
            GRID_LINES);;
  init_tiles();
}

void init(void)
{
  /* init_sdl(); */
  init_xcb();
}

void cleanup(void)
{
  delete_grid();
  SDL_DestroyWindow(options.window);
  SDL_Quit();
}


void update(void)
{
  /* menu options: */
  /*  -new map */
  /*  -load map */
  /*  -manage assets */
  /*  -keymaps */
}


static void test_mode(void)
{
  SDL_SetRenderDrawColor(options.renderer,
                         255, 0, 0, SDL_ALPHA_OPAQUE);
  SDL_RenderClear(options.renderer);

  draw_grid();

  SDL_RenderPresent(options.renderer);
}

void render(void)
{
  /* test_mode(); */
  
}
