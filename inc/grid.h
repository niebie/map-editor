/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of map-editor. */

/* map-editor is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* map-editor is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with map-editor.  If not, see <http://www.gnu.org/licenses/>. */

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>

#define GRID_WIDTH 10
#define GRID_HEIGHT 10
#define TILE_WIDTH 75
#define TILE_HEIGHT 75
#define TILE_DIV 2

extern struct grid_item **grid;

void init_grid(SDL_Renderer *renderer, int x, int y, int tw, int th, int options);
void delete_grid(void);

void set_tile(int x, int y, char *texture_file);

void draw_grid(void);

void grid_click_down(SDL_Event *e);
void grid_click_move(SDL_Event *e);
void grid_click_up(SDL_Event *e);

#define GRID_LINES 0x01
