SOURCES=\
	./src/common.c \
	./src/main.c \
	./src/grid.c \
	./src/event_loop.c \
	./src/core.c \
	./src/xcb.c
INCLUDES=\
	./inc/common.h \
	./inc/core.h \
	./inc/event_loop.h \
	./inc/grid.h \
	./inc/xcb_util.h
CCFLAGS=-I/usr/include/SDL2
LDFLAGS=-lSDL2 -lxcb -lxcb-keysyms

.PHONY:clean all

all:clean emap

clean:
	-rm ./emap

emap:$(SOURCES) $(INCLUDES)
	gcc $(SOURCES) $(CCFLAGS) $(LDFLAGS) -o emap
